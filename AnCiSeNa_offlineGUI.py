"""
   Copyright 2022 Marek Hanus

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

# import of librares
import numpy as np
import cv2
import csv
import datetime
time2 = datetime.datetime.now()

import os
import glob

import tkinter
from tkinter import *
from tkinter.ttk import *

from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from tkscrolledframe import ScrolledFrame
import pandas as pd
import shutil

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#implemantion of Tkinter
root = Tk()

class GUI():
    def init(self):
        # creating application window
        root.title("AnCiSeNa offline app")
        root.geometry('900x500')
        
        sf = ScrolledFrame(root)
        sf.pack(side = "bottom",expand=1, fill="both") 
        
        sf.bind_arrow_keys(root)
        sf.bind_scroll_wheel(root)
        
        self.win = Gtk.Window()
        self.window = sf.display_widget(Frame)
        
        self.lbl = Label(self.window, text="AnČiSeNa offline application")
        self.lbl.grid(column=0, row=0)
        s = Style()
        s.theme_use('clam')
        s.configure("red.Horizontal.TProgressbar", foreground='blue', background='blue')
        
    
        self.progress = Progressbar(root, orient=HORIZONTAL, length = 900, mode = 'determinate', style="red.Horizontal.TProgressbar")
        self.progress.pack(pady = 10, side=LEFT)
        
        
        self.btnVyhod = Button(self.window, text="Vyhodnotit fotky", command=self.VyhodnotButton)
        self.btnVyhod.grid(column=2, row=0)
        self.btnGraf = Button(self.window, text="Zobrazit graf z csv", command=self.GrafButton)
        self.btnGraf.grid(column=3, row=0)
        self.btnStahovani = Button(self.window, text="Stáhnout fotky", command=self.predstahovani)
        self.btnStahovani.grid(column=1, row=0)
        
        # set running directory 
        self.slozka = os.getcwd()
        
    def vyhodnoceni_form(self, filename):
        kriteria = []
        if os.path.exists("vyhodnoceni_sett.csv") == True:
            with open("vyhodnoceni_sett.csv", mode = 'r') as f:
                csvfile = csv.reader(f)
                for lines in csvfile:
                    kriteria = lines
        else:
            with open("vyhodnoceni_sett.csv", mode = 'w+') as f:
                writer = csv.writer(f)
                writer.writerow("")
                
            
        self.fileWindow = tkinter.Toplevel(self.window)
        self.stabilLabel = Label(self.fileWindow,   text="  Stabilní pozice průměrné hodnoty:")
        self.hladinaLabel = Label(self.fileWindow,  text=" Násobitel detekce hladiny:           ")
        self.sedimentLabel = Label(self.fileWindow, text=" Násobitel detekce sedimentu:      ")
        self.konecLabel = Label(self.fileWindow,    text=" Násobitel detekce konce vzorku:  ")
        
        self.stabilLabel.grid(row=0, column=0)
        self.hladinaLabel.grid(row=1, column=0)
        self.sedimentLabel.grid(row=2, column=0)
        self.konecLabel.grid(row=3, column=0, columnspan=2)
        
        self.stabilEntry, self.hladinaEntry, self.sedimentEntry, self.konecEntry  = Entry(self.fileWindow), Entry(self.fileWindow),Entry(self.fileWindow),Entry(self.fileWindow)
        
        vsechnyEntry = [self.stabilEntry, self.hladinaEntry, self.sedimentEntry, self.konecEntry]
        for i in range(4):
            vsechnyEntry[i].grid(row=i, column=2)
            vsechnyEntry[i].insert(i,kriteria[i])
            
            
        def preset():
            kriteria = [self.stabilEntry.get()]
            kriteria.append(self.hladinaEntry.get())
            kriteria.append(self.sedimentEntry.get())
            kriteria.append(self.konecEntry.get())
            with open("vyhodnoceni_sett.csv", mode = 'w+') as f:
                writer = csv.writer(f)
                writer.writerow(kriteria)
                
            presetLabel = Label(self.fileWindow,   text="Nastavení uloženo")
            presetLabel.grid(row=9, column=1)
         
        self.button2= Button(self.fileWindow, text="Ulož nastavení", command=preset)
        self.button2.grid(row=9, column=2)
        self.button= Button(self.fileWindow, text="Ok", command=lambda: self.vyhodnoceni(filename,self.stabilEntry.get(),self.hladinaEntry.get(),self.sedimentEntry.get(),self.konecEntry.get()))
        self.button.grid(row=9, column=0)
        
        

    def GrafButton(self):
        filename = tkinter.filedialog.askopenfile(parent=root,mode='rb',title='Vyber soubor')
        self.CSVGraf(filename)
    
    def VyhodnotButton(self):
        filename = tkinter.filedialog.askdirectory()
        self.vyhodnoceni_form(filename)
        
    def CSVGraf(self,filename): # format data from CSV to chart
        df = pd.read_csv(filename, sep=",", encoding='utf16')
        y1 = []
        y2 = []
        y3 = []
        y4 = []
        y5 = []
        y6 = []
        y7 = []
        y8 = []
        y9 = []
        
        for i in df["sediment1"]:
            y2.append(i)
        for i in df["sediment2"]:
            y4.append(i)
        for i in df["sediment3"]:
            y6.append(i)
            
        for i in df["celkove1"]:
            y1.append(i)
        for i in df["celkove2"]:        
            y3.append(i)
        for i in df["celkove3"]:
            y5.append(i)
        
        for i in df["procenta1"]:
            y7.append(i)
        for i in df["procenta2"]:
            y8.append(i)
        for i in df["procenta3"]:
            y9.append(i)
        
        self.graf(y7,y8,y9)
        
    
    def predstahovani(self):
        varovani = tkinter.messagebox.askquestion('Varování', 'Tvoje staré vyhodnocení pod názvem "AnCiSeNa" bude smazáno. :)')
        if varovani =='yes':
            kriteria = []
            if os.path.exists("vyhodnoceni_sett.csv") == True:
                with open("vyhodnoceni_sett.csv", mode = 'r') as f:
                    csvfile = csv.reader(f)
                    for lines in csvfile:
                        kriteria = lines
            else:
                with open("vyhodnoceni_sett.csv", mode = 'w+') as f:
                    writer = csv.writer(f)
                    writer.writerow("")
            
            self.fileWindow = tkinter.Toplevel(self.window)
            self.fileLabel = Label(self.fileWindow, text="Vyber složku: /home/pi/Documents/AnCiSeNa/static/")
            self.enterBox = Entry(self.fileWindow, text="AnCiSeNa_fotky")
            self.enterBox.grid(row=0, column=2)
            self.enterBox.insert(0,"AnCiSeNa_fotky")
            self.fileLabel.grid(row=0, column=0)
            
            self.ip_label = Label(self.fileWindow, text="Vyber IP adresu:")
            self.ip_label.grid(row=1, column=0)
            self.ipenterBox = Entry(self.fileWindow, text="192.168.1.4")
            self.ipenterBox.grid(row=1, column=2)
            self.ipenterBox.insert(0,"192.168.1.4")
            
            
            self.stabilLabel = Label(self.fileWindow,   text="  Stabilní pozice průměrné hodnoty:")
            self.hladinaLabel = Label(self.fileWindow,  text=" Násobitel detekce hladiny:           ")
            self.sedimentLabel = Label(self.fileWindow, text=" Násobitel detekce sedimentu:      ")
            self.konecLabel = Label(self.fileWindow,    text=" Násobitel detekce konce vzorku:  ")
            
            self.stabilLabel.grid(row=2, column=0)
            self.hladinaLabel.grid(row=3, column=0)
            self.sedimentLabel.grid(row=4, column=0)
            self.konecLabel.grid(row=5, column=0)
            
            self.stabilEntry, self.hladinaEntry, self.sedimentEntry, self.konecEntry  = Entry(self.fileWindow), Entry(self.fileWindow),Entry(self.fileWindow),Entry(self.fileWindow)
            
            vsechnyEntry = [self.stabilEntry, self.hladinaEntry, self.sedimentEntry, self.konecEntry]
            for i in range(4):
                vsechnyEntry[i].grid(row=i+2, column=2)
                vsechnyEntry[i].insert(i,kriteria[i])
            
            def preset():
                kriteria = [self.stabilEntry.get()]
                kriteria.append(self.hladinaEntry.get())
                kriteria.append(self.sedimentEntry.get())
                kriteria.append(self.konecEntry.get())
                with open("vyhodnoceni_sett.csv", mode = 'w+') as f:
                    writer = csv.writer(f)
                    writer.writerow(kriteria)
                    
                presetLabel = Label(self.fileWindow,   text="Nastavení uloženo")
                presetLabel.grid(row=9, column=1)
            
            self.button2= Button(self.fileWindow, text="Ulož nastavení", command=preset)
            self.button2.grid(row=9, column=2)
            self.button= Button(self.fileWindow, text="Ok", command=lambda: self.stahovani(self.ipenterBox.get(),self.enterBox.get(),self.stabilEntry.get(),self.hladinaEntry.get(),self.sedimentEntry.get(),self.konecEntry.get()))
            self.button.grid(row=9, column=0)
                
            
            
    def stahovani(self, adresa, slozka, stabilHOD, hladinaHOD, sedimentHOD,konecHOD):
        newfile = slozka
        self.fileWindow.destroy()
        self.fileWindow.update()
        self.lbl.config(text="Stahování obrázků...")
        root.update_idletasks()
        
        try:
            shutil.rmtree(self.slozka+"/AnCiSeNa")
        except OSError as e:
            pass
        os.mkdir(self.slozka+"AnCiSeNa")
        os.system('scp pi@'+str(adresa)+':/home/pi/Documents/AnCiSeNa/static/'+newfile+" "+self.slozka)
        self.lbl.config(text="Vyhodnocování obrázků...")
        self.vyhodnoceni(self.slozka+"/AnCiSeNa")
        self.CSVGraf(self.slozka+"/AnCiSeNa/csv_offline.csv")
        
    def graf(self, y1,y2,y3):
        x = []
        for xko in range((len(y1)-1)):
            x.append(xko+1)
            
        fig = Figure()
        fig.set_figwidth(12)
        fig.set_figheight(10)
        axs = fig.add_subplot(1,1,1)
        
        fig.suptitle('Graf levého vzorku')
        axs.plot(x, y1[1:], label = "procenta1")
        axs.plot(x,y2[1:], label = "procenta 2")
        axs.plot(x, y3[1:], color="green", label='procenta 3')
        fig.legend()
        
        line = FigureCanvasTkAgg(fig, self.window)
        line.get_tk_widget().grid(row=1, column=0, columnspan = 200)
    
    def vyhodnoceni(self, slozka, stabilHOD, hladinaHOD, sedimentHOD,konecHOD):
        try: 
            self.fileWindow.destroy()
        except:
            pass
        
        self.s2 = Style()
        self.s2.theme_use('clam')
        self.s2.configure("blue.Horizontal.TProgressbar", foreground='green', background='green')
        self.progress["style"] = "blue.Horizontal.TProgressbar"
        
        fotecky = []
        fotky = []
        directory = slozka
        for file in glob.glob(r''+directory+''"**/*.jpg", recursive=True):
            fotecky.append(os.path.join(r''+directory,file))
        
        fotky = sorted(fotecky)
        
        with open(directory+'/csv_offline.csv', 'w+', newline='', encoding='utf16') as csvfile:
            fieldnames = ["cas"]
            for i in range(3):
                fieldnames.append("celkove"+str(i+1))   
                fieldnames.append("sediment"+str(i+1))
                fieldnames.append("procenta"+str(i+1))
                fieldnames.append("gray"+str(i+1))
            writer = csv.writer(csvfile)
            writer.writerow(fieldnames)
        
                #sediment = celkovykonec
                #vzorek = zacatekvody
        pocetobrazku = len(fotky)
        celkovykonecprumer = 0
        celkovyzacatekprumer = 0
        for velkei in range(len(fotky)):
            img = cv2.imread(fotky[velkei])
            blue, green, red = cv2.split(img)
            
            sirkaKamery = 2592
            vyskaKamery = 1944
        
            image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        
            # *****************************************************************************************************************
            poleprumeru = [0]*(sirkaKamery+1)
        
            cyclenum = 0
            cropped_image = [0]*3
            cropped_image[0] = img[0:1944, 100:740]
            cropped_image[1] = img[0:1944, 1000:1645]
            cropped_image[2] = img[0:1944, 1895:2500]
            sirkaKamery = [640,645,605]
            exporty = [str(velkei+1)]
            
            
            
            for kolo in range(3):
                img = cropped_image[kolo]
                image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
                cyclenum = 0
                
                poleprumeru = [0]*(vyskaKamery+1)
                
                nejvyssiprumer = 0
                nejvyssikonec = 0
                for g in gray:
                    
                    cyclenum = cyclenum +1
                    prumer = []
                    
                    for l in range(395, 405):
                       prumer.append(g[l])
                       
                    poleprumeru[cyclenum] = round(np.average(prumer),1)
                    if(nejvyssiprumer < poleprumeru[cyclenum] and cyclenum > 700 and cyclenum < 1200):
                        nejvyssiprumer = poleprumeru[cyclenum]
                        nejvyssiprumer_cycle = cyclenum
                        
                    if(nejvyssikonec < poleprumeru[cyclenum] and cyclenum > 1100 and cyclenum < 1350):
                        nejvyssikonec = poleprumeru[cyclenum]
                zacatek = 0
                polesede = []
                if velkei == 0: 
                    kriteriumKonce = ((nejvyssikonec - poleprumeru[int(stabilHOD)])*float(konecHOD))+poleprumeru[int(stabilHOD)]
                    celkovykonec = 0
                    for k in range(1200, vyskaKamery):
                        if(poleprumeru[k] > kriteriumKonce):
                            celkovykonec = k
                            celkovykonecprumer = celkovykonec
                            break
                    
                else:
                    celkovykonec = celkovykonecprumer
                
                pozice_sedimentu = celkovykonec-50
                
                kriteriumSedimentu = ((nejvyssiprumer - poleprumeru[int(stabilHOD)])*float(sedimentHOD))+poleprumeru[int(stabilHOD)]
                kriteriumHladiny = ((nejvyssiprumer - poleprumeru[int(stabilHOD)])*float(hladinaHOD))+poleprumeru[int(stabilHOD)]
                
                sedaRadek = 0
                sedaRadekZacatek = 1097
                sedaRadekKonec = 11551
                poziceZacatek = 250
                poziceKonec = 350
                for p in gray:
                    seda = []
                    pozice = 0
                    for o in p:
                        if(sedaRadek > sedaRadekZacatek and sedaRadek < sedaRadekKonec and pozice > poziceZacatek and pozice < poziceKonec):
                            if(o != "NaN"):
                                seda.append(o)
                        pozice = pozice+1
                    if(sedaRadek > sedaRadekZacatek+1 and sedaRadek < sedaRadekKonec):
                        polesede.append(np.average(seda))
                    sedaRadek = sedaRadek+1
                celaseda = int(np.average(polesede))
                
                if kolo == 0:
                    kriteriumSedimentu = 145.5
                for h in range(1200,nejvyssiprumer_cycle-50,-1):
                    if(poleprumeru[h] > kriteriumSedimentu):
                        zacatek = h
                        break
                
                
                
                
                
                
                
                nejvyssiPrumer_check = 0
                zacatekvody = 0
                if velkei == 0:
                    for j in range(zacatek, 0, -1):
                        if(poleprumeru[j] < kriteriumHladiny and nejvyssiPrumer_check == 1):
                            zacatekvody = j
                            celkovyzacatekprumer = zacatekvody
                            break
                        if(poleprumeru[j] == nejvyssiprumer):
                            nejvyssiPrumer_check =1
                          
                else:
                    zacatekvody = celkovyzacatekprumer
                
                
                sediment = celkovykonec  - zacatek
                vzorek = celkovykonec - zacatekvody
                
                if(vzorek == 0):
                    sedimentProcenta = 0
                else:
                    sedimentProcenta = sediment*100/vzorek
                exporty.append(str(vzorek))
                exporty.append(str(sediment))
                exporty.append(round(sedimentProcenta,2))
                exporty.append(str(celaseda)) 
            with open(directory+'/csv_offline.csv', 'a', newline='', encoding='utf16') as csvfile:
                fieldnames = [""]
                writer = csv.writer(csvfile)
                writer.writerow(exporty)
                
            hodnota = ((velkei+1) * 100)/pocetobrazku
            self.progress["value"] = round(hodnota,2)
            self.lbl.config(text="obr. "+str((velkei+1))+" z "+str(pocetobrazku)+" "+str(round(hodnota,2))+" %")
            root.update_idletasks()
        
        self.CSVGraf(directory+"/csv_offline.csv")
        
    
    
    
if __name__ == '__main__':
    app = GUI()
    app.init()
    root.mainloop()
   
