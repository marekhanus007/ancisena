"""
Analyzátor čistoty a sedimantace nanodiamantů - Grayscale + Sedimentace

Created on Sat Feb  5 13:21:23 2022

@author: Marek Hanus
"""

import numpy as np
import csv

import datetime
import time

import threading
import http.server

from picamera.array import PiRGBArray
from picamera import PiCamera
import picamera

from flask import Flask, request, render_template, redirect
import cv2
import pandas as pd
import os
import shutil




COLOR = (255,180,5) #05B4FF  - color of photo text
font = cv2.FONT_HERSHEY_SIMPLEX
font_size = 2
font_color = COLOR
font_thickness = 2
pruchod = False
stop_b = False
ZIPY = False
nazev = ["Vzorek 1", "Vzorek 2", "Vzorek 3"]
nazevSEDI = ["","",""]
sedimentace_opakovani = 0
sedimentace_timer = 0
filename = "AnCiSeNa_fotky"
filename2 = "AnCiSeNa_"

sedimentace = 0
sedaRadekZacatek = 1097
sedaRadekKonec = 1155
poziceZacatek = 250
poziceKonec = 350
vyskaKamery = 1944
sirkaKamery = 2592

# ****************************************************************************************************************************************************
# **************************************************************Creating webserver by flask***********************************************************
# ****************************************************************************************************************************************************
def zapis_nazev():
        global nazev
        with open('static/nazev_csvcko.csv', 'w+', newline='', encoding='utf16') as csvfile:
            writer = csv.writer(csvfile)
            zprava = [nazev[0],nazev[1],nazev[2]]
            writer.writerow(zprava)

zapis_nazev()

app = Flask(__name__) 
app.config['TEMPLATES_AUTO_RELOAD'] = True

def start_flask():
    def zapis_status(text):
        with open('static/status.csv', 'w+', newline='', encoding='utf16') as csvfile:
            writer = csv.writer(csvfile)
            zprava = [text]
            writer.writerow(zprava)
    
    def zapis_nazev():
        global nazev
        with open('static/nazev_csvcko.csv', 'w+', newline='', encoding='utf16') as csvfile:
            writer = csv.writer(csvfile)
            zprava = [nazev[0],nazev[1],nazev[2]]
            writer.writerow(zprava)
    
    @app.route('/')
    def first_form():
        global sedimentace
        if sedimentace == 0:
            return render_template('AnCiSeNa_grayscale.html')
        else:
            return render_template('AnCiSeNa_sedimentace.html')
    
    @app.route('/', methods=['POST'])
    def grayscale_post():
        global nazev
        nazev[0] = request.form['nazev1']
        nazev[1] = request.form['nazev2']
        nazev[2] = request.form['nazev3']
        zapis_nazev()
        return redirect("/")
    
    @app.route('/start/')
    def my_form():
        global pruchod
        pruchod = False
        return render_template('AnCiSeNa_start.html')
    
    @app.route('/start/', methods=['POST'])
    def sent_data():
        global sedimentace_opakovani
        global sedimentace_timer
        global pruchod
        global ZIPY
        global nazevSEDI
        pruchod = True 
        sedimentace_opakovani = request.form['text']
        sedimentace_timer = request.form['text2']
        nazevSEDI[0] = request.form['nazev1']
        nazevSEDI[1] = request.form['nazev2']
        nazevSEDI[2] = request.form['nazev3']
        ZIPY = request.form['ZIPY']
        print("hodnoty z startu ", sedimentace_opakovani, sedimentace_timer, ZIPY)
        return redirect("/") 
    
    @app.route('/stop/')
    def StopFunction():
        print('Stopuju...')
        global pruchod
        global sedimentace
        pruchod = False
        sedimentace = 0
        zapis_status("Stopuji...")
        
        
        return redirect("/")
    
    
    
    @app.route('/sedimentace/')
    def SedimentaceFunction():
        global sedimentace
        sedimentace = 1
        return redirect("/")
    
    @app.route('/grayscale/')
    def GrayscaleFunction():
        global sedimentace
        sedimentace = 0
        return redirect("/")
    
    @app.route('/static/')
    def my_form_post2():
        return render_template('AnCiSeNa_obrazky.html')
    
    @app.route('/vysledky/')
    def my_form_post3():
        return render_template('AnCiSeNa_vysledky.html')
    
    @app.route('/graf/')
    def my_form_post4():
        global nazevSEDI
        return render_template('plt_chart.html', Vzorek1=nazevSEDI[0], Vzorek2=nazevSEDI[1], Vzorek3=nazevSEDI[2])
    
    if __name__ == '__main__':
        app.run(host="192.168.1.57",port=5000)
        
    
f = threading.Thread(target=start_flask) # starting flask as subprocess
f.start()


#***************************************************************************************************************************************************
#*********************************************************************** Camera settings ***********************************************************
#***************************************************************************************************************************************************
camera = PiCamera()
camera.rotation = 180
camera.resolution= (2592,1944)
camera.capture('prvni.jpg')


#***************************************************************************************************************************************************
#**************************************************************************Grayscale****************************************************************
#***************************************************************************************************************************************************
#***************************************************************************************************************************************************
def Grayscale():
    print("grayscale")
    with open('static/gray_csvcko.csv', 'w+', newline='', encoding='utf16') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow("")
    
    while(sedimentace == 0):
        camera.capture('static/gray_image2.jpg')
        time.sleep(1)
        img = cv2.imread('static/gray_image2.jpg')
        blue, green, red = cv2.split(img)
    
    
        image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        cv2.imwrite("static/AnCiSeNa_fotky/grayscale_image.jpg", gray)
        
        
        
        # ***************************************************************************************************************** 
        poleprumeru = [0]*(sirkaKamery+1)
        cropped_image = [0]*3
        
        
        # pixel positions of samples
        cropped_image[0] = img[0:1944, 100:740]
        cropped_image[1] = img[0:1944, 1000:1645]
        cropped_image[2] = img[0:1944, 1895:2500]
        
        timer = datetime.datetime.now()
        cas = timer.strftime("%H:%M:%S")
        datetime2 = timer.strftime("%d.%m.%Y %H:%M:%S")
        exporty = [cas]
        #***********************************************************************************************************
        for kolo in range(3):
            
            img = cropped_image[kolo]
            image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
            cyclenum = 0
            
            poleprumeru = [0]*(vyskaKamery+1)
            nejvyssiprumer = 0
            nejvyssikonec = 0
            
            for g in gray:
                cyclenum = cyclenum +1
                prumer = []
                for i in range(395, 405):
                   prumer.append(g[i])
                poleprumeru[cyclenum] = round(np.average(prumer),1)
                
                if(nejvyssiprumer < poleprumeru[cyclenum] and cyclenum > 200 and cyclenum < 1200):
                    nejvyssiprumer = poleprumeru[cyclenum]
                    
                if(nejvyssikonec < poleprumeru[cyclenum] and cyclenum > 1100 and cyclenum < 1350):
                    nejvyssikonec = poleprumeru[cyclenum]
            sedaRadek = 0
            polesede = []
            
            for p in gray:
                seda = []
                pozice = 0
                for o in p:
                    if(sedaRadek > sedaRadekZacatek and sedaRadek < sedaRadekKonec and pozice > poziceZacatek and pozice < poziceKonec):
                        if(o != "NaN"):
                            seda.append(o)
                    pozice = pozice+1
                if(sedaRadek > sedaRadekZacatek+1 and sedaRadek < sedaRadekKonec):
                    polesede.append(np.average(seda))
                sedaRadek = sedaRadek+1
            celaseda = int(np.average(polesede))
                
            exporty.append(str(celaseda))
        img2 = cv2.imread('static/gray_image2.jpg')
        
        textG = ["sedost:", str(exporty[1]), str(exporty[2]), str(exporty[3])]
        textN = ["vzorek:", str(nazev[0]), str(nazev[1]), str(nazev[2])]
        textT = "datum & cas: " + datetime2
        x = [20,270,1150,2100]
        y = [100,200,1900]
        for i in range(4):
            delka =  len(str(nazev[i-1]))
            if i > 0 and delka > 3:
                plus = (len(str(nazev[i-1])) * 18) /2
            else:
                plus = 0
            x2 = x[i] + int(plus)
            print(plus)
            cv2.putText(img2, textG[i], (x2,y[1]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
            
        
        for i in range(4):
            cv2.putText(img2, textN[i], (x[i],y[0]), font, font_size, font_color, font_thickness, cv2.LINE_AA)        
        cv2.putText(img2, textT, (x[0],y[2]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
        cv2.imwrite("static/gray_image.jpg",img2)
        with open('static/gray_csvcko.csv', 'a', newline='', encoding='utf16') as csvfile: 
                                     
            writer = csv.writer(csvfile)
            
            writer.writerow(exporty)



#***************************************************************************************************************************************************
#************************************************************************Sedimentace****************************************************************
#***************************************************************************************************************************************************
#***************************************************************************************************************************************************

def Sedimentace():
    print("sedimentace")
    
    def zapis_status(text):
        with open('static/status.csv', 'w+', newline='', encoding='utf16') as csvfile:
            writer = csv.writer(csvfile)
            zprava = [text]
            writer.writerow(zprava)
    
    zapis_status("Čekám na start...")
    global pruchod
    while(pruchod == False and sedimentace==1):
        pass
    if(sedimentace == 1):
        time.sleep(0.2)
        opakovani_pomocna = 0
        if(int(sedimentace_opakovani) == 0):
            opakovani_text = "nekonečno"
        else:
            opakovani_text = sedimentace_opakovani
        
        zapis_status("Nastartováno... Opakovaní: "+str(opakovani_text)+"; Opakování: "+str(sedimentace_timer)+" Zipovat:"+str(ZIPY))
        time.sleep(1)
        
        def start_init():
            shutil.rmtree('static/'+filename)
            time.sleep(0.5)
            os.makedirs('static/'+filename)
            time.sleep(0.1)
            
            with open('static/'+filename+'/'+filename+'.csv', 'w+', newline='', encoding='utf16') as csvfile:
                fieldnames = ["cas"]
                for i in range(3):
                    fieldnames.append(nazevSEDI[i]+"_celkove")   
                    fieldnames.append(nazevSEDI[i]+"_sediment")
                    fieldnames.append(nazevSEDI[i]+"_procenta")
                    fieldnames.append(nazevSEDI[i]+"_gray")
                writer = csv.writer(csvfile)
                writer.writerow(fieldnames)

        start_init()
        
        cislokola = 1
        timerik = datetime.datetime.now()
        
        
        if(int(sedimentace_opakovani) == 0):
            opakovani_pomocna = float('inf')
        else:
            opakovani_pomocna = int(sedimentace_opakovani)
    celkovykonecprumer = []
    celkovyzacatekprumer = []
    while(sedimentace == 1 and cislokola <= opakovani_pomocna and pruchod == True):
        prvnicas = time.perf_counter()
        zapis_status("Fotím...")
        
        cas = timerik.strftime("%H:%M:%S")
        datetime2 = timerik.strftime("%d.%m.%Y %H:%M:%S")
        filetime = timerik.strftime("%Y%m%d_%H%M%S")
        print("jo3")
        if cislokola < 10:
            filenumber = "_000"+str(cislokola)
        elif cislokola < 100:
            filenumber = "_00"+str(cislokola)
        elif cislokola < 1000:
            filenumber = "_0"+str(cislokola)
        else:
            filenumber = "_"+str(cislokola)
            
        camera.capture('static/'+filename+'/'+filename2+filetime+filenumber+'.jpg')
        time.sleep(0.1)
        img = cv2.imread('static/'+filename+'/'+filename2+filetime+filenumber+'.jpg')
        blue, green, red = cv2.split(img)
    
        image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        
        
        # *****************************************************************************************************************
        zapis_status("Analyzuji...")
        poleprumeru = [0]*(sirkaKamery+1)
        
        cyclenum = 0
        
        cropped_image = [0]*3
        
        
        cropped_image[0] = img[0:1944, 100:740]
        cropped_image[1] = img[0:1944, 1000:1645]
        cropped_image[2] = img[0:1944, 1895:2500]
        timerik = datetime.datetime.now()
        
        exporty = [datetime2]
        #***********************************************************************************************************
        for kolo in range(3):
            
            img = cropped_image[kolo]
            image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
            
            cyclenum = 0
            
            poleprumeru = [0]*(vyskaKamery+1)
            nejvyssiprumer = 0
            nejvyssikonec = 0
            
            for g in gray:
                cyclenum = cyclenum +1
                prumer = []
                for i in range(395, 405):
                   prumer.append(g[i])
                poleprumeru[cyclenum] = round(np.average(prumer),1)
                
                if(nejvyssiprumer < poleprumeru[cyclenum] and cyclenum > 700 and cyclenum < 1200):
                    nejvyssiprumer = poleprumeru[cyclenum]
                    #nejvyssiprumer_cislo = cyclenum
                    
                if(nejvyssikonec < poleprumeru[cyclenum] and cyclenum > 1100 and cyclenum < 1350):
                    nejvyssikonec = poleprumeru[cyclenum]
            
            zacatek = 0
            ocko = 0
            polesede = []
            
            celkovykonec = 0
            
            kriteriumKonce = ((nejvyssikonec - poleprumeru[1100])*0.8)+poleprumeru[1100]
            if cislokola < 10:
                for k in range(1200, vyskaKamery):
                    if(poleprumeru[k] > kriteriumKonce):
                        celkovykonec = k
                        break
                celkovykonecprumer.append(celkovykonec)
            else:
                celkovykonec = int(np.average(celkovykonecprumer))
                
            pozice_sedimentu = celkovykonec-50
            
            kriteriumSedimentu = ((nejvyssiprumer - poleprumeru[pozice_sedimentu])*0.5)+poleprumeru[pozice_sedimentu]
            kriteriumHladiny = ((nejvyssiprumer - poleprumeru[pozice_sedimentu])*0.85)+poleprumeru[pozice_sedimentu]
            
            for p in gray:
                seda = []
                for o in p:
                    if(ocko > 600 and ocko < 800):
                        if(o != "NaN"):
                            seda.append(o)
                    
                if(ocko > 600 and ocko < 800):
                    polesede.append(np.average(seda))
                ocko = ocko+1
                
            celaseda = int(np.average(polesede))
            
            for h in range(1200,0,-1):
                if(poleprumeru[h] > kriteriumSedimentu):
                    zacatek = h
                    break
            
            nejvyssiprumer_check = 0
            if cislokola < 10:
                for j in range(zacatek, 0, -1):
                    if(poleprumeru[j] < kriteriumHladiny and nejvyssiprumer_check == 1):
                        zacatekvody = j
                        break
                    if(poleprumeru[j] == nejvyssiprumer):
                        nejvyssiprumer_check = 1
                celkovyzacatekprumer.append(zacatekvody)
            else:
                zacatekvody = int(np.average(celkovyzacatekprumer))
            
    
            sediment = celkovykonec - zacatek
            dopis = celkovykonec - zacatekvody 
            sedimentProcenta = sediment*100/dopis
            exporty.append(str(dopis))
            exporty.append(str(sediment))
            exporty.append(round(sedimentProcenta,2))
            exporty.append(str(celaseda))
            
        #****************************************************
        # Creating picture to webserver
        
        img2 = cv2.imread('static/'+filename+'/'+filename2+filetime+filenumber+'.jpg')
        
        textG = ["sedost:", str(exporty[4]), str(exporty[8]), str(exporty[12])]
        textN = ["vzorek:", str(nazevSEDI[0]), str(nazevSEDI[1]), str(nazevSEDI[2])]
        textS = ["sediment:",str(exporty[3])+ "%", str(exporty[7])+ "%", str(exporty[11])+ "%"]
        textT = "datum & cas: " + datetime2
        textC = "cycle: " + str(cislokola)
        
        x = [20,350,1100,2000]
        y = [100,200,1800,1900]
        
        
        
        for i in range(4):
            cv2.putText(img2, textN[i], (x[i],y[0]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
            cv2.putText(img2, textS[i], (x[i],y[1]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
            cv2.putText(img2, textG[i], (x[i],y[2]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
        
        cv2.putText(img2, textT, (x[0],y[3]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
        cv2.putText(img2, textC, (x[3],y[3]), font, font_size, font_color, font_thickness, cv2.LINE_AA)
        
        cv2.imwrite("static/webserver_photo.jpg",img2)
        
        with open('static/'+filename+'/'+filename+'.csv', 'a', newline='', encoding='utf16') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(exporty)
            
        zapis_status("Vyhodnoceno, čekám na timer...")
        print("namereno, vyhodnoceno, cekam na timer :)")
    
        df = pd.read_csv("static/"+filename+"/"+filename+".csv", encoding='utf-16', sep=',')
        df.to_html('templates/AnCiSeNa_vysledky.html', index=False)
        os.system("cd static")
        os.system('tree -P "*.jpg|*.csv" -H ./'+filename+' -o templates/AnCiSeNa_obrazky.html static/'+filename+'*')
        cislokola=cislokola+1
        
        novycas = time.perf_counter()
        
        while((prvnicas + int(sedimentace_timer)) > novycas and sedimentace == 1):
            novycas = time.perf_counter()
            time.sleep(0.1)
        
        print("konec hlavniho loopu")
    
    
    
    pruchod = False
    if ZIPY == "ANO":
        zapis_status("Vytvářím ZIP...")
        os.system('zip -9jpr static/'+filename+'/'+filename+'.zip static/'+filename) # create zip
        os.system('tree -P "*.jpg|*.zip|*.csv" -H ./'+filename+' -o templates/AnCiSeNa_obrazky.html static/'+filename+'*') #create tree html
    zapis_status("Měření skončilo, čekám na nastartování")
    while(pruchod == False and sedimentace == 1):
        print("sedimentace dojela")
        
        time.sleep(1)


#***************************************************************************************************************************************************
#************************************************************************* Main loop ***************************************************************
#***************************************************************************************************************************************************
def main():
    while(1):
        if sedimentace == 0:
            Grayscale()
        else:
            Sedimentace()
            time.sleep(1)

main()











